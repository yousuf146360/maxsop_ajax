<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ajax Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Country List</h2> 
  <input type="text" name="search" id="search" placeholder="Search Here.." class="form-control"> 
  <table class="table">
    <thead>
      <tr>
        <th>Sl</th>
        <th>Country Code</th>
        <th>Country Name</th>       
      </tr>
    </thead>
    <tbody id="country_list">               
    </tbody>
  </table>
</div>

<script type="text/javascript">
$(document).ready(function(){ 
  
    var data = country = [];   
    $.ajax({
        type : "POST",
        url  : "http://localhost/ajax_test/function.php",
        data : ""
    }).done(function(response){       
        var data = JSON.parse(response);        
        $.each(data,function(i,el){
         country.push("<tr><td>" + (i+1) + "</td><td>" + el.country_code + "</td><td>" + el.country_name + "</td></tr>");
        });   

        $('#country_list').html(country);     
    });
});  
</script>
</body>
</html>
